#!/usr/bin/env python3

import numpy as np
from skimage import io
import propagator

path = '../plates/Placa_Zonal_1.tif'
source_field = io.imread(path)#, cv2.IMREAD_GRAYSCALE)
source_field = np.array(source_field, dtype=float)

# I work with cm
propagator = propagator.fresnelPropagator(source_field, 550e-7, 2)
propagator.propagate(2000)
