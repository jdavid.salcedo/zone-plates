#!/usr/bin/env python3
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib

# Wrapper functions for 2-dimensional Fourier transform and inverse Fourier
# transform
def fourier_transform(array):
    ''' array is supposed to be the image of function that takes two
    spatial arguments, x and y
    '''
    FT = np.fft.fft2(np.fft.fftshift(array))
    return FT

def inverse_fourier_transform(array):
    ''' array is supposed to be the image of function that takes two
    frequency arguments, freq_x and freq_y
    '''
    IFT = np.fft.ifftshift(np.fft.ifft2(array))
    return IFT

class fresnelPropagator:
    ''' Standard propagator class based on transfer function; assumes same
    horizontal and vertical side lengths and uniform sampling
    '''
    def __init__(self, source_field, wavelength, side):
        # source field must be an array
        self.source_field = source_field
        self.wavelength = wavelength
        self.side = side
        # self.distance = distance

    def propagate(self, distance):
        dx = self.side/len(self.source_field) # space resolution
        # square frequency domain complying with the Nyquist-Shannon sampling
        # theorem
        frequencies = np.arange(-1/(2*dx), 1/(2*dx),
                                1/(self.side))
        freq_x, freq_y = np.meshgrid(frequencies, frequencies)

        transfer_function = np.fft.fftshift(
            np.exp(-complex(0,1) * np.pi * self.wavelength * distance *
            (freq_x**2 + freq_x**2)))*dx**2
        source_fourier_transform = fourier_transform(self.source_field)

        # computational definition of the Fresnel propagator
        observed_field = inverse_fourier_transform(
            source_fourier_transform*transfer_function)
        intensity = np.abs(observed_field)**2

        plt.imshow(intensity, cmap='gray', extent=[-self.side/2, self.side/2,
                                                   -self.side/2, self.side/2])
        plt.show()
        #return observed_field
